package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func getRouter() *gin.Engine {
	r := gin.Default()

	// Set protected routes
	protected := r.Group("/")
	//protected.Use(verifyApiKey())
	getProtectedRoutes(protected)

	// Set unprotected routes
	unprotected := r.Group("/")
	getUnprotectedRoutes(unprotected)
	r.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, "The page was not found.")
	})
	return r
}

func getProtectedRoutes(g *gin.RouterGroup) {
	// Get routes

	// POST routes
	g.POST("/logout")

	// DELETE routes

	// PUT routes
}

func getUnprotectedRoutes(g *gin.RouterGroup) {
	// GET routes
	g.GET("/login")
	g.GET("/status")

	// POST routes
	g.POST("/login/action")
}
