package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// verifyApiKey middleware to verify if api key provided in the header exists
func verifyApiKey() gin.HandlerFunc {
	return func(c *gin.Context) {
		apiKey := c.Request.Header.Get("jwt")
		if apiKey == "" {
			c.AbortWithStatusJSON(http.StatusForbidden, "User could not be verified")
			return
		}
		//exists := handlers.Repo.DB.VerifyApiKeyExists(apiKey)
		//if !exists {
		//	c.AbortWithStatusJSON(http.StatusForbidden, "User could not be verified")
		//	return
		//}
	}
}
